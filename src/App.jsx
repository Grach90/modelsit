import "./App.css";
import Navbar from "./Components/navbar/Navbar.jsx";
import Routes from "./Components/routes/AllRoutes.jsx";
import Footer from "./Components/footer/footer.jsx";
import { Helmet } from "react-helmet";
function App() {
  return (
    <div className="App">
      <Helmet>
        <script type="application/ld+json">
          {`
          {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "ModelsIT ",
            "url": "https://modelsit.com/",
            "logo": "https://modelsit.com/static/images/company_logo.png",
            "image": "https://modelsit.com/static/images/company_logo.png",
            "description": "ModelsIT provides custom software development and IT outsourcing services. Our developers build custom software for individuals, startups, and small to medium business.",
            "contactPoint": {
              "@type": "ContactPoint",
              "telephone": "+374 94 724349",
              "contactType": "customer service"
            }
          }
          `}
        </script>
      </Helmet>
      <Navbar />
      <div className="container">
        <Routes />
      </div>
      <Footer />
    </div>
  );
}

export default App;
