import Styles from "./footer.module.css";
import mobile from "../../static/images/mobile.svg";
import envelop from "../../static/images/envelop.svg";
import location from "../../static/images/location.svg";

const Footer = () => {
  return (
    <footer className={Styles.footer}>
      <div className={Styles.about}>
        <h3>About us</h3>
        <p>
          ModelsIT is a software development company. We provide comprehensive
          services including development of mobile applications, web-oriented
          applications, business software solutions, as well as software
          integration and updating, support, and maintenance of software
          applications
        </p>
      </div>
      <div className={Styles.businessArea}>
        <h3>Business areas</h3>
        <p>Web Application Development</p>
        <p>Business Application Development</p>
        <p>Online Configurator</p>
        <p>Design Automation</p>
      </div>
      <div className={Styles.contacts}>
        <h3>Contacts</h3>
        <div className={Styles.mobile}>
          <img src={mobile} alt="mobile icon" />
          <p>+374 94 724349</p>
        </div>
        <div className={Styles.envelop}>
          <img src={envelop} alt="envelop icon" />
          <p>info@modelsit.com</p>
        </div>
        <div className={Styles.envelop}>
          <img src={location} alt="envelop icon" />
          <p>Armenia, City of Vanadzor, 12 Shinararneri str</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
