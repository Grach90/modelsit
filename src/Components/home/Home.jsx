import Styles from "./home.module.css";

import { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";

import { useRef } from "react";
import emailjs from "@emailjs/browser";
import TechnologiesPage from "../technologies/Technologies.jsx";

import BackgroundImage from "../../static/images/homePageImage.png";
import Responsibility from "../../static/images/s1.png";
import Professionalism from "../../static/images/s2.png";
import Efficiency from "../../static/images/s3-1.png";
import Communication from "../../static/images/s4.png";
import DevProgress from "../../static/images/developing_progress.png";
import ICN from "../../static/images/ICN.jpg";
import Ardesk from "../../static/images/Ardesk.png";
import EIF from "../../static/images/EIF.png";
import VTC from "../../static/images/VTC.png";
import BCK from "../../static/images/BCK.png";
import KD from "../../static/images/KD.png";
import ArchiTek from "../../static/images/ArchiTek.png";

const HomePage = () => {
  const [errorMessage, setErrorMessage] = useState("");
  const [successMessage, setSuccessMessage] = useState("");

  const formRef = useRef();

  const sendEmail = (values, { resetForm } = {}) => {
    emailjs
      .sendForm(
        "service_4y99485",
        "template_yva063p",
        formRef.current,
        "6nK01KVJkpXjrUM3D"
      )
      .then(
        (result) => {
          setSuccessMessage(
            "Thank you for your inquiry, we will definitely get back to you."
          );
          resetForm();
        },
        (error) => {
          setErrorMessage("Something went wrong, please try again later");
        }
      );
  };

  const formik = useFormik({
    initialValues: {
      user_name: "",
      user_email: "",
      message: "",
    },
    validationSchema: Yup.object({
      user_name: Yup.string().required(),
      user_email: Yup.string().email().required(),
      message: Yup.string().required(),
    }),
    onSubmit: sendEmail,
  });

  const handleClick = (e) => {
    e.preventDefault();
    if (
      Object.keys(formik.errors).length > 0 ||
      !Object.values(formik.values).some((value) => value)
    ) {
      setErrorMessage(
        " Please check if you've filled all the fields with valid information. Thank you."
      );
    } else {
      setErrorMessage("");
    }
    return formik.handleSubmit();
  };

  return (
    <div className={Styles.mainDiv}>
      <div className={Styles.backgroundImage}>
        <img src={BackgroundImage} alt="" width="100%" />
      </div>
      <div className={Styles.TellUs}>
        <div className={Styles.aboutCompany}>
          <h1>ModelsIT is a Custom Software Development Company</h1>
          <p>
            We provide custom software development and IT outsourcing services.
            Our developers build custom software for individuals, startups, and
            small to medium business.
          </p>
        </div>
        <div className={Styles.form}>
          <h2>
            Need a Custom Software? <br /> Tell us about Your project
          </h2>
          <form ref={formRef}>
            <input
              className={formik.errors.user_name && Styles.error}
              type="text"
              placeholder="*Name"
              name="user_name"
              onChange={formik.handleChange}
              value={formik.values.user_name}
            />
            <input
              className={formik.errors.user_email && Styles.error}
              type="text"
              placeholder="*Email"
              name="user_email"
              onChange={formik.handleChange}
              value={formik.values.user_email}
            />
            <textarea
              className={formik.errors.message && Styles.error}
              placeholder="*Describe your idea"
              name="message"
              onChange={formik.handleChange}
              value={formik.values.message}
            ></textarea>
            {errorMessage && (
              <div className={Styles.errorMessage}>
                <span>{errorMessage}</span>
              </div>
            )}
            {successMessage && (
              <div className={Styles.successMessage}>
                <span>{successMessage}</span>
              </div>
            )}
            <button onClick={handleClick}>Get a FREE Quote</button>
          </form>
        </div>
      </div>
      <div className={Styles.shouldChoose}>
        <h1>Why should you choose ModelsIT?</h1>
        <div className={Styles.advantages}>
          <div className={Styles.advantagesCol}>
            <h2>Responsibility</h2>
            <div className={Styles.advantagesImg}>
              <img src={Responsibility} alt="" />
            </div>
            <p>
              ModelsIT takes responsibility for the quality of its services, for
              the expediency and soundness of the decisions made in the
              development process, for accomplishing the tasks, as well as for
              the confidence reposed by our customers, providing guaranteed
              maintenance of our software products.
            </p>
          </div>
          <div className={Styles.advantagesCol}>
            <h2>Professionalism</h2>
            <div className={Styles.advantagesImg}>
              <img src={Professionalism} alt="" />
            </div>
            <p>
              This means, we strive to carefully perform our work, based on deep
              insights into processes, in-depth expertise, compliance with
              security requirements, and ensuring full customer confidentiality.
            </p>
          </div>
          <div className={Styles.advantagesCol}>
            <h2>Efficiency</h2>
            <div className={Styles.advantagesImg}>
              <img src={Efficiency} alt="" />
            </div>
            <p>
              We efficiently distribute time and material resources throughout
              the project, leading to the development of a competitive software
              product to make the customer proud of our work.
            </p>
          </div>
          <div className={Styles.advantagesCol}>
            <h2>Communication</h2>
            <div className={Styles.advantagesImg}>
              <img src={Communication} alt="" />
            </div>
            <p>
              We strive to keep in touch with our customers, let them know about
              every stage of the development process and appreciate feedback,
              because trust between teams and customers is the basis of
              excellent teamwork.
            </p>
          </div>
        </div>
      </div>
      {/* <div className={Styles.gallery}>
        <h1>We are trusted by</h1>
        <ul className={Styles.cards}>
          <li>
            <img src={ICN} style={{ width: "50%" }} alt="ICN" />
          </li>
          <li>
            <img src={EIF} alt="ICN" />
          </li>
          <li>
            <img src={Ardesk} alt="ICN" />
          </li>
          <li>
            <img src={VTC} alt="ICN" />
          </li>
          <li>
            <img src={BCK} alt="ICN" />
          </li>
          <li>
            <img src={ArchiTek} alt="ICN" />
          </li>
          <li>
            <img src={KD} alt="ICN" />
          </li>
        </ul>
      </div> */}
      <div className={Styles.developmentCycle}>
        <h1>Our Software Development Cycle</h1>
        <div>
          <p>
            We apply a systematic approach to complex and simple projects. We
            follow Agile methodology, Scrum and Kanban, which allow our
            dedicated team to deliver exceptional apps on time.
          </p>
          <img src={DevProgress} alt="" />
        </div>
      </div>
      <TechnologiesPage />
    </div>
  );
};

export default HomePage;
