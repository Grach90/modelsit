import { Routes, Route } from "react-router-dom";
import HomePage from "../home/Home.jsx";
import AboutPage from "../about/About.jsx";
import TechnologiesPage from "../technologies/Technologies.jsx";
import ProductPage from "../product/Product.jsx";

const AllRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/about" element={<AboutPage />} />
      <Route path="/technologies" element={<TechnologiesPage />} />
      <Route path="/scene" element={<ProductPage />} />
    </Routes>
  );
};

export default AllRoutes;
