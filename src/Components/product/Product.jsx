import Styles from "./product.module.css";
import zplan from "../../static/images/z-plan.gif";
import walk from "../../static/images/walk.gif";
import view from "../../static/images/view.gif";
import measure from "../../static/images/measure.gif";

const ProductPage = () => {
  return (
    // <section className={Styles.section}>
    //   <div className={Styles.wrapper}>
    //     <div className={Styles.technologiItem}>
    //       <h1>3D Scene in the Browser</h1>
    //       <p>
    //         3D scene is a 3D space in the browser where it is possible to open
    //         3D models and at the moment in the 3D scene it is possible to open
    //         Autodesk Inventor and Autodesk Revit files. We offer companies that
    //         use Autodesk Inventor and Autodesk Revit programs for their products
    //         to place a 3D scene with the company's products on the company's web
    //         page. Technically, 3D scene integration is harmless to any web page
    //         using any technology and the integration process is much faster than
    //         it might seem. There are many tools in the 3d scene, but below are
    //         two of them.
    //       </p>
    //       <img src={view} alt="" />
    //     </div>
    //     <div className={Styles.technologiItem}>
    //       <h1>Walking</h1>
    //       <img src={walk} alt="" />
    //     </div>
    //     <div className={Styles.technologiItem}>
    //       <h1>Section Analysis</h1>
    //       <img src={zplan} alt="" />
    //     </div>
    //     <div className={Styles.technologiItem}>
    //       <h1>Measure</h1>
    //       <img src={measure} alt="" />
    //     </div>
    //   </div>
    // </section>
    <div style={{ position: "relative" }}>
      <div className={Styles.header}></div>
      <iframe
        style={{ width: "100%", height: "100vh", marginTop: "15px" }}
        src="https://aps-extensions.autodesk.io/"
        frameborder="0"
      ></iframe>
    </div>
  );
};

export default ProductPage;
