import Styles from "./navbar.module.css";
import mobile from "../../static/images/mobile.svg";
import envelop from "../../static/images/envelop.svg";
import baselineMenuBlack from "../../static/images/baselineMenuBlack.png";
import baselineCloseBlack from "../../static/images/baselineCloseBlack.png";
import logo from "../../static/images/company_logo.png";

import { NavLink } from "react-router-dom";
import { useState } from "react";
import cn from "classnames";

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const handleOpenToggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <nav className={Styles.nav}>
      <div className={Styles.bottomNavbar}>
        <div className={Styles.burger} onClick={handleOpenToggle}>
          {isOpen ? (
            <img src={baselineCloseBlack} alt="baselineMenuBlack" />
          ) : (
            <img src={baselineMenuBlack} alt="baseLineCloseBlacg" />
          )}
        </div>
        <NavLink to="/" className={Styles.logo}>
          <img src={logo} alt="logo icon" />
        </NavLink>
        <ul
          className={cn(!isOpen ? Styles.headerList : Styles.headerListMobile)}
        >
          <li>
            <NavLink onClick={() => setIsOpen(false)} to="/about">
              ABOUT US
            </NavLink>
          </li>
          <li>
            <NavLink onClick={() => setIsOpen(false)} to="/technologies">
              TECHNOLOGIES
            </NavLink>
          </li>
          <li>
            <NavLink onClick={() => setIsOpen(false)} to="/scene">
              AUTODESK VIEWER
            </NavLink>
          </li>
        </ul>
        <div className={Styles.contactContainer}>
          <div className={Styles.mobile}>
            <img src={mobile} alt="mobile icon" />
            <span>+374 94 724349</span>
          </div>
          <div className={Styles.envelop}>
            <img src={envelop} alt="envelop icon" />
            <span>info@modelsit.com</span>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
