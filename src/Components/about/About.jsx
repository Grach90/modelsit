import Styles from "./about.module.css";

import { useEffect, useRef } from "react";
import emailjs from "@emailjs/browser";

import { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";

const AboutPage = () => {
  const [errorMessage, setErrorMessage] = useState("");
  const [successMessage, setSuccessMessage] = useState("");

  const formRef = useRef();

  const sendEmail = (values, { resetForm }) => {
    emailjs
      .sendForm(
        "service_4y99485",
        "template_yva063p",
        formRef.current,
        "6nK01KVJkpXjrUM3D"
      )
      .then(
        (result) => {
          setSuccessMessage(
            "Thank you for your inquiry, we will definitely get back to you."
          );
          resetForm();
        },
        (error) => {
          setErrorMessage("Something went wrong, please try again later");
        }
      );
  };

  const formik = useFormik({
    initialValues: {
      user_name: "",
      user_email: "",
      message: "",
    },
    validationSchema: Yup.object({
      user_name: Yup.string().required(),
      user_email: Yup.string().email().required(),
      message: Yup.string().required(),
    }),
    onSubmit: sendEmail,
  });

  const handleClick = (e) => {
    e.preventDefault();
    if (
      Object.keys(formik.errors).length > 0 ||
      !Object.values(formik.values).some((value) => value)
    ) {
      setErrorMessage(
        " Please check if you've filled all the fields with valid information. Thank you."
      );
    } else {
      setErrorMessage("");
    }
    formik.handleSubmit();
  };

  return (
    <section className={Styles.section}>
      <div className={Styles.wrapper}>
        <h1 className={Styles.aboutUsCover}>About Us </h1>
        <div className={Styles.aboutContent}>
          <p>
            ModelsIT offers custom software development and IT outsourcing
            services. Our developers create customized software for individuals,
            start-ups and small and medium-sized businesses.
          </p>
          <p>
            Our implemented projects have been used successfuly and we continue
            to setisfy our customers' demands which is of prime importance. We
            had different orders for small and large projects, and we
            successfully completed all tasks.
          </p>
          <p>
            Based on years of experience, we know that each business has a
            different software and hardware environment. That is why we provide
            a wide range of software development services, as well as to meet
            the needs and requirements of customers for the most modern
            technologies.
          </p>
          <h1 className={Styles.contactUsCover}>Contact us</h1>
          <div>
            <form ref={formRef}>
              <div className={Styles.formContainer}>
                <div className={Styles.contactsInput}>
                  <input
                    className={formik.errors.user_name && Styles.error}
                    type="text"
                    placeholder="*Name"
                    name="user_name"
                    onChange={formik.handleChange}
                    value={formik.values.user_name}
                  />
                </div>
                <div className={Styles.contactsInput}>
                  <input
                    type="text"
                    placeholder="Company"
                    name="company"
                    onChange={formik.handleChange}
                    value={formik.values.company}
                  />
                </div>
                <div className={Styles.contactsInput}>
                  <input
                    className={formik.errors.user_email && Styles.error}
                    type="text"
                    placeholder="*E-mail"
                    name="user_email"
                    onChange={formik.handleChange}
                    value={formik.values.user_email}
                  />
                </div>
                <div className={Styles.contactsInput}>
                  <input
                    type="text"
                    placeholder="Phone"
                    name="phone"
                    onChange={formik.handleChange}
                    value={formik.values.phone}
                  />
                </div>
                <div className={Styles.contactsTextArea}>
                  <textarea
                    className={formik.errors.message && Styles.error}
                    type="text"
                    placeholder="*How can we help you"
                    name="message"
                    onChange={formik.handleChange}
                    value={formik.values.message}
                  />
                </div>
                {errorMessage && (
                  <div className={Styles.errorMessage}>
                    <span>{errorMessage}</span>
                  </div>
                )}
                {successMessage && (
                  <div className={Styles.successMessage}>
                    <span>{successMessage}</span>
                  </div>
                )}
                <div className={Styles.sendButton}>
                  <button onClick={handleClick}>Send</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AboutPage;
