import { Text, Svg } from "@react-three/drei";
import { Node } from "./Nodes.jsx";
import ReactLogo from "../../static/images/react-js-icon.svg";
import NestLogo from "../../static/images/nest-js-icon.svg";
import WebLogo from "../../static/images/web.svg";
import AwsLogo from "../../static/images/aws-icon.svg";
import NextLogo from "../../static/images/nextjs-icon.svg";
import ThreeLogo from "../../static/images/threejs-1.svg";
import GitLogo from "../../static/images/git-icon.svg";
import MongoLogo from "../../static/images/mongodb-icon.svg";
import PostgresLogo from "../../static/images/postgresql.svg";
import AndroidLogo from "../../static/images/android-6.svg";
import IosLogo from "../../static/images/apple.svg";
import RvtLogo from "../../static/images/rvt-icon-home.svg";
import CLogo from "../../static/images/c--4.svg";
import NETLogo from "../../static/images/NET.svg";

export const ReactNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        React
      </Text>
      <Svg src={ReactLogo} scale={0.005} position={[-0.3, 0.27, 1]} />
    </>
  );
};
export const NestNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Nest JS
      </Text>
      <Svg src={NestLogo} scale={0.0015} position={[-0.35, 0.35, 1]} />
    </>
  );
};
export const WebNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Web
      </Text>
      <Svg src={WebLogo} scale={0.0015} position={[-0.38, 0.39, 1]} />
    </>
  );
};
export const AwsNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Amazon
      </Text>
      <Svg src={AwsLogo} scale={0.0000023} position={[-0.39, 0.23, 1]} />
    </>
  );
};
export const NextNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Next JS
      </Text>
      <Svg src={NextLogo} scale={0.0018} position={[-0.46, 0.46, 1]} />
    </>
  );
};
export const ThreeNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        THREE JS
      </Text>
      <Svg src={ThreeLogo} scale={0.0035} position={[-0.35, 0.37, 1]} />
    </>
  );
};
export const GitNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Git
      </Text>
      <Svg src={GitLogo} scale={0.0055} position={[-0.33, 0.35, 1]} />
    </>
  );
};
export const MongoNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Mongo DB
      </Text>
      <Svg src={MongoLogo} scale={0.00065} position={[-0.2, 0.43, 1]} />
    </>
  );
};
export const PostgresNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Postgre Sql
      </Text>
      <Svg src={PostgresLogo} scale={0.003} position={[-0.38, 0.36, 1]} />
    </>
  );
};
export const AndroidNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Android
      </Text>
      <Svg src={AndroidLogo} scale={0.0035} position={[-0.46, 0.64, 1]} />
    </>
  );
};
export const IosdNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        IOS
      </Text>
      <Svg src={IosLogo} scale={0.0008} position={[-0.35, 0.4, 1]} />
    </>
  );
};
export const RVTNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        Revit API
      </Text>
      <Svg src={RvtLogo} scale={0.0035} position={[-0.27, 0.4, 1]} />
    </>
  );
};
export const CNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        C#
      </Text>
      <Svg src={CLogo} scale={0.003} position={[-0.38, 0.42, 1]} />
    </>
  );
};
export const NETNode = () => {
  return (
    <>
      <Text position={[0, -0.9, 1]} fontSize={0.25} fontWeight={900}>
        .NET
      </Text>
      <Svg src={NETLogo} scale={0.007} position={[-0.447, 0.449, 1]} />
    </>
  );
};
