import Styles from "./technologies.module.css";

import { useState, createRef } from "react";
import { Canvas } from "@react-three/fiber";
import { Nodes, Node } from "./Nodes.jsx";

import {
  ReactNode,
  WebNode,
  NestNode,
  AwsNode,
  NextNode,
  ThreeNode,
  GitNode,
  MongoNode,
  PostgresNode,
  AndroidNode,
  IosdNode,
  RVTNode,
  CNode,
  NETNode,
} from "./NodesContent.jsx";

import { useThree } from "@react-three/fiber";
import { useEffect } from "react";
// import { log } from "three/src/nodes/TSL.js";

// const useAdaptiveZoom = (defaultZoom = 80) => {
//   const { size, camera } = useThree();

//   useEffect(() => {
//     const { width, height } = size;
//     // Adjust zoom based on the width of the viewport
//     const aspectRatio = width / height;
//     console.log("aspectRatio:", aspectRatio);
//     // Decrease zoom for larger devices, increase zoom for smaller ones
//     const oppositeZoom = defaultZoom * Math.max(aspectRatio, 1) * 1.06;
//     camera.zoom = oppositeZoom;
//     camera.updateProjectionMatrix(); // Ensure the camera updates with the new zoom
//   }, [size, camera, defaultZoom]);

//   return camera.zoom;
// };
const useAdaptiveZoom = (defaultZoom = 80) => {
  const { size, camera } = useThree();

  useEffect(() => {
    const { width, height } = size;

    // Determine the smaller dimension and scale inversely
    const minDimension = Math.min(width, height);
    const adjustedZoom = (defaultZoom * minDimension) / 1000; // Scale based on a reference size

    // Apply the adjusted zoom and update the camera
    camera.zoom = adjustedZoom;
    camera.updateProjectionMatrix();
  }, [size.width, size.height, camera, defaultZoom]);

  return camera.zoom;
};

const TechnologiesPage = () => {
  const [
    [
      web,
      react,
      nest,
      aws,
      next,
      git,
      mongo,
      three,
      postgre,
      android,
      ios,
      rvt,
      c,
      net,
    ],
  ] = useState(() => [...Array(14)].map(createRef));
  const nodeRefs = {
    web,
    react,
    nest,
    aws,
    next,
    git,
    mongo,
    three,
    postgre,
    android,
    ios,
    rvt,
    c,
    net,
  };
  const position = {
    web: {
      l: [0, 0, 0],
      s: [0.67, -0.9, 0],
    },
    react: {
      l: [-7, 0, 0],
      s: [-4.7, 2.5, 0],
    },
    nest: {
      l: [7, 0, 0],
      s: [2.6, 2.9, 0],
    },
    aws: {
      l: [7, 4, 0],
      s: [-2, 4.4, 0],
    },
    next: {
      l: [-2, -5, 0],
      s: [4.4, -1.4, 0],
    },
    git: {
      l: [-6, 5, 0],
      s: [2.9, -5.5, 0],
    },
    mongo: {
      l: [2, -5.2, 0],
      s: [-0.75, 9.4, 0],
    },
    three: {
      l: [-13, -5, 0],
      s: [-5, -6, 0],
    },
    postgre: {
      l: [0.3, 4.6, 0],
      s: [4.3, 9.4, 0],
    },
    rvt: {
      l: [12, 4, 0],
      s: [4.6, 5.6, 0],
    },
    android: {
      l: [-13, 4.5, 0],
      s: [-3.3, -3, 0],
    },
    ios: {
      l: [-13, 0, 0],
      s: [-4.8, 9.2, 0],
    },
    net: {
      l: [12, -5, 0],
      s: [-0.75, -6.8, 0],
    },
    c: {
      l: [12, 0, 0],
      s: [4.1, 0.6, 0],
    },
  };
  const [width, setWidth] = useState(window.innerWidth);
  const [height, setHeight] = useState(window.innerHeight);
  useEffect(() => {
    const handleResize = (next) => {
      setWidth(next.target.innerWidth);
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <div className={Styles.section}>
      <Canvas orthographic color="#fff">
        <AdaptiveZoom defaultZoom={60} />
        <Nodes>
          <Node
            ref={web}
            color="#fff"
            position={width > 650 ? position.web.l : position.web.s}
          >
            <WebNode />
          </Node>

          <Node
            ref={react}
            color="#fff"
            position={width > 650 ? position.react.l : position.react.s}
            connectedTo={[web]}
          >
            <ReactNode />
          </Node>

          <Node
            ref={nest}
            color="#fff"
            position={width > 650 ? position.nest.l : position.nest.s}
            connectedTo={[web]}
          >
            <NestNode />
          </Node>

          <Node
            ref={aws}
            color="#fff"
            position={width > 650 ? position.aws.l : position.aws.s}
            connectedTo={[web, nest]}
          >
            <AwsNode />
          </Node>

          <Node
            ref={next}
            color="#fff"
            position={width > 650 ? position.next.l : position.next.s}
            connectedTo={[react]}
          >
            <NextNode />
          </Node>

          <Node
            ref={git}
            color="#fff"
            position={width > 650 ? position.git.l : position.git.s}
            connectedTo={[web, aws, react]}
          >
            <GitNode />
          </Node>

          <Node
            ref={mongo}
            color="#fff"
            position={width > 650 ? position.mongo.l : position.mongo.s}
            connectedTo={[git, nest, net]}
          >
            <MongoNode />
          </Node>

          <Node
            ref={three}
            color="#fff"
            position={width > 650 ? position.three.l : position.three.s}
            connectedTo={[web, next, react]}
          >
            <ThreeNode />
          </Node>

          <Node
            ref={postgre}
            color="#fff"
            position={width > 650 ? position.postgre.l : position.postgre.s}
            connectedTo={[web, nest, aws]}
          >
            <PostgresNode />
          </Node>

          <Node
            ref={c}
            color="#fff"
            position={width > 650 ? position.c.l : position.c.s}
            connectedTo={[rvt, net]}
          >
            <CNode />
          </Node>

          <Node
            ref={rvt}
            color="#fff"
            position={width > 650 ? position.rvt.l : position.rvt.s}
            connectedTo={[aws]}
          >
            <RVTNode />
          </Node>

          <Node
            ref={android}
            color="#fff"
            position={width > 650 ? position.android.l : position.android.s}
            connectedTo={[react]}
          >
            <AndroidNode />
          </Node>

          <Node
            ref={ios}
            color="#fff"
            position={width > 650 ? position.ios.l : position.ios.s}
            connectedTo={[react]}
          >
            <IosdNode />
          </Node>

          <Node
            ref={net}
            color="#fff"
            position={width > 650 ? position.net.l : position.net.s}
            connectedTo={[web]}
          >
            <NETNode />
          </Node>
        </Nodes>
      </Canvas>
    </div>
  );
};
const AdaptiveZoom = ({ defaultZoom }) => {
  useAdaptiveZoom(defaultZoom);
  return null;
};
export default TechnologiesPage;
